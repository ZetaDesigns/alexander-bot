#!/usr/bin/env python3
import yaml
import discord
from discord.ext import commands
from discord.ext.commands import has_permissions, MissingPermissions, has_role
import os
from cogs.db import db_login
config = yaml.safe_load(open('config.yml'))
token = yaml.safe_load(open('token.yml'))
bot = commands.Bot(config['prefix'],description='')

bot.loaded_cogs = []

db_login()

def load_cogs():
    for extension in os.listdir('cogs'):
        if os.path.isfile('cogs/{}'.format(extension)) and extension.endswith('.py'):
            try:
                bot.load_extension("cogs." + extension[:-3])
                bot.loaded_cogs.append(extension[:-3])
            except Exception as e:
                print(e)
            else:
                print('Erweiterung {}'.format(extension) + ' geladen.') 

load_cogs()

@has_role("Operarius")
@bot.command()
async def list_cogs(ctx):
    '''Gibt alle Erweiterungen zurück'''
    cog_list = commands.Paginator(prefix='', suffix='')
    cog_list.add_line('Geladen:')
    for cog in bot.loaded_cogs:
        cog_list.add_line('- ' + cog)

    for page in cog_list.pages:
        await ctx.send(page)

@has_role("Operarius")
@bot.command()
async def load(ctx, cog):
    '''Versuche die ausgewählte Erweiterung zu laden'''
    if cog in bot.loaded_cogs:
        return await ctx.send('Erweiterung wurde bereits geladen.')
    try:
        bot.load_extension('cogs.{}'.format(cog))
    except Exception as exc:
        await ctx.send('```{}\n{}```'.format(type(exc).__name__, exc))
        await ctx.send('⚠ Laden der Erweiterung fehlgeschlagen.')
    else:
        bot.loaded_cogs.append(cog)
        await ctx.send('Die Erweiterung wurde erfolgreich geladen.')

@has_role("Operarius")
@bot.command()
async def unload(ctx, cog):
    if cog not in bot.loaded_cogs:
        return await ctx.send('Die Erweiterung war noch gar nicht geladen.')
    bot.unload_extension('cogs.{}'.format((cog)))
    bot.loaded_cogs.remove(cog)
    await ctx.send('Die Erweiterung wurde erfolgreich deaktiviert.')

@has_role("Operarius")
@bot.command()
async def shutdown(ctx):
        await ctx.send("Stoppe.")
        print("Stoppe auf Befehl von {}.".format(ctx.author.name))
        await bot.close()
        await bot.logout()
        await bot.wait_closed()
        os.exit(0)
@has_role("Operarius")
@bot.command()
async def reload(ctx, cog):
    if cog not in bot.loaded_cogs:
        return await ctx.send('Die Erweiterung war noch gar nicht geladen.')
    bot.unload_extension('cogs.{}'.format((cog)))
    try:
        bot.load_extension('cogs.{}'.format(cog))
        await ctx.send('Die Erweiterung wurde erfolgreich neu geladen.')
    except Exception as exc:
        await ctx.send('```{}\n{}```'.format(type(exc).__name__, exc))
        await ctx.send('⚠ Laden der Erweiterung fehlgeschlagen.')
    

@shutdown.error
@unload.error
@load.error
@reload.error
async def cmderror(ctx, error):
    if isinstance(error, commands.MissingPermissions):
        await ctx.send("Dir fehlen die nötigen Berechtigungen um diesen Befehl auszuführen.")

@bot.listen
async def on_ready():
    print('Eingeloggt als: ' + bot.user.name)

bot.run(token["token"])