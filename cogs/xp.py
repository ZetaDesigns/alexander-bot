from discord.ext import commands
import discord
from .db import db_execute, db_get
import re
import asyncio
import math

class XP(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        exclude_channels = ["entwickler-chat", "tabularium", "senatorenpalast"]
        guild = discord.utils.get(self.bot.guilds, id=message.guild.id)
        if message.author.id != 500349811879444500 and message.channel.name not in exclude_channels and message.content.startswith(self.bot.command_prefix) == False:
            Nachricht = message.content
            xp=5
            if len(Nachricht)>15:
                xp=xp+4
            if len(Nachricht)>25:
                xp=xp+2
            currentxp = db_get("SELECT xp FROM benutzer WHERE id = " + str(message.author.id))
            prevlvl = 0
            totalxp = 0
            username = message.author.name
            pattern = re.compile('\W')
            username = re.sub(pattern, '', username)
            if currentxp != []:
                totalxp = currentxp[0][0] + xp
                newlvl = int(round(119843.9 + (1.321281 - 119843.9)/(1 + math.pow(totalxp/187744599999.99997,0.4918001))))
                
                if newlvl >= 12:
                    role = discord.utils.get(guild.roles, name="Signifier")
                    await message.author.add_roles(role)
                if newlvl >= 25:
                    role = discord.utils.get(guild.roles, name="Centurio")
                    await message.author.add_roles(role)
                if newlvl >= 35:
                    role = discord.utils.get(guild.roles, name="Patrizier")
                    await message.author.add_roles(role)
                if newlvl >= 75:
                    role = discord.utils.get(guild.roles, name="Volkstribun")
                    await message.author.add_roles(role)
                db_execute("REPLACE INTO benutzer (username, id, xp, level) VALUES ('" + username + "', " + str(message.author.id) + ", " + str(totalxp) + "," + str(newlvl) + ")")
            else:
                db_execute("REPLACE INTO benutzer (username, id, xp, level) VALUES ('" + username + "', " + str(message.author.id) + ", " + str(xp) + ", 1)")

def setup(bot):
    bot.add_cog(XP(bot))
