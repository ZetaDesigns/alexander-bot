from discord.ext import commands
import discord
import random
import os
import yaml

class Memes(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(pass_context=True)
    async def zitat(self, ctx):
        if ctx.channel.name == "forum":
            await ctx.message.delete()
            return await ctx.author.send("Dieser Befehl kann im Forum nicht genutzt werden.")
        elif ctx.invoked_subcommand is None:

            rapper = zitate['Zitate'][random.randint(0,len(zitate['Zitate'])-1)]
            zitat = rapper[next(iter(rapper))][random.randint(0,len(rapper[next(iter(rapper))])-1)]

            text = ''
            for zeile in zitat['text']:
                text += zeile + '\n'

            embed = discord.Embed(colour=discord.Colour(0xD4AF02), description=text)
            embed.set_image(url=zitat['bild'][0])
            embed.set_footer(text=zitat['name'][0])

            await ctx.send(embed=embed)
        
    @zitat.command(pass_context=True)
    async def moneyboy(self, ctx):
        rapper = zitate['Zitate'][0]
        zitat = rapper[next(iter(rapper))][random.randint(0,len(rapper[next(iter(rapper))])-1)]
        text = ''
        for zeile in zitat['text']:
           text += zeile + '\n'

        embed = discord.Embed(colour=discord.Colour(0xD4AF02), description=text)

        embed.set_image(url=zitat['bild'][0])
        embed.set_footer(text=zitat['name'][0])

        await ctx.send(embed=embed)
    
    @zitat.command(pass_context=True, aliases=['farid'])
    async def faridbang(self, ctx):
        rapper = zitate['Zitate'][1]
        zitat = rapper[next(iter(rapper))][random.randint(0,len(rapper[next(iter(rapper))])-1)]
        text = ''
        for zeile in zitat['text']:
           text += zeile + '\n'

        embed = discord.Embed(colour=discord.Colour(0xD4AF02), description=text)

        embed.set_image(url=zitat['bild'][0])
        embed.set_footer(text=zitat['name'][0])

        await ctx.send(embed=embed)
    
    @zitat.command(pass_context=True, aliases=['kolle'])
    async def kollegah(self, ctx):
        rapper = zitate['Zitate'][2]
        zitat = rapper[next(iter(rapper))][random.randint(0,len(rapper[next(iter(rapper))])-1)]
        text = ''
        for zeile in zitat['text']:
           text += zeile + '\n'

        embed = discord.Embed(colour=discord.Colour(0xD4AF02), description=text)

        embed.set_image(url=zitat['bild'][0])
        embed.set_footer(text=zitat['name'][0])

        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Memes(bot))
    with open("cogs/zitate.yml", encoding='utf8') as f:
        global zitate
        zitate = yaml.safe_load(f)