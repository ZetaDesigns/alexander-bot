from discord.ext import commands
import discord
class Gamerole(commands.Cog):
	def __init__(self, bot):
		self.bot = bot

	@commands.Cog.listener()
	async def on_raw_reaction_add(self, payload):
		if payload.channel_id == 497512266636197889:
			emoji = str(payload.emoji)
			guild = discord.utils.get(self.bot.guilds, id=payload.guild_id)
			categoryrole = discord.utils.get(guild.roles, id=519222299170308105)
			await discord.utils.get(guild.members, id=payload.user_id).add_roles(categoryrole)
			if emoji =='1⃣':
				role = discord.utils.get(guild.roles, name="CS:GO")
				await discord.utils.get(guild.members, id=payload.user_id).add_roles(role)
			elif emoji =='2⃣':
				role = discord.utils.get(guild.roles, name="GTA V")
				await discord.utils.get(guild.members, id=payload.user_id).add_roles(role)
			elif emoji =='3⃣':
				role = discord.utils.get(guild.roles, name="Minecraft")
				await discord.utils.get(guild.members, id=payload.user_id).add_roles(role)
			elif emoji =='4⃣':
				role = discord.utils.get(guild.roles, name="Rocket League")
				await discord.utils.get(guild.members, id=payload.user_id).add_roles(role)
			elif emoji =='5⃣':
				role = discord.utils.get(guild.roles, name="Garrys Mod")
				await discord.utils.get(guild.members, id=payload.user_id).add_roles(role)
			elif emoji =='6⃣':
				role = discord.utils.get(guild.roles, name="Overwatch")
				await discord.utils.get(guild.members, id=payload.user_id).add_roles(role)
			elif emoji =='7⃣':
				role = discord.utils.get(guild.roles, name="Blobby Volley")
				await discord.utils.get(guild.members, id=payload.user_id).add_roles(role)
			elif emoji =='8⃣':
				role = discord.utils.get(guild.roles, name="CIV 6")
				await discord.utils.get(guild.members, id=payload.user_id).add_roles(role)
			elif emoji =='9⃣':
				role = discord.utils.get(guild.roles, name="Flash Games")
				await discord.utils.get(guild.members, id=payload.user_id).add_roles(role)
			elif emoji =='🔟':
				role = discord.utils.get(guild.roles, name="League of Legends")
				await discord.utils.get(guild.members, id=payload.user_id).add_roles(role)
    
	@commands.Cog.listener()	
	async def on_raw_reaction_remove(self, payload):
		if payload.channel_id == 497512266636197889:
			emoji = str(payload.emoji)
			guild = discord.utils.get(self.bot.guilds, id=payload.guild_id)
			if emoji =='1⃣':
				role = discord.utils.get(guild.roles, name="CS:GO")
				await discord.utils.get(guild.members, id=payload.user_id).remove_roles(role)
			elif emoji =='2⃣':
				role = discord.utils.get(guild.roles, name="GTA V")
				await discord.utils.get(guild.members, id=payload.user_id).remove_roles(role)
			elif emoji =='3⃣':
				role = discord.utils.get(guild.roles, name="Minecraft")
				await discord.utils.get(guild.members, id=payload.user_id).remove_roles(role)
			elif emoji =='4⃣':
				role = discord.utils.get(guild.roles, name="Rocket League")
				await discord.utils.get(guild.members, id=payload.user_id).remove_roles(role)
			elif emoji =='5⃣':
				role = discord.utils.get(guild.roles, name="Garrys Mod")
				await discord.utils.get(guild.members, id=payload.user_id).remove_roles(role)
			elif emoji =='6⃣':
				role = discord.utils.get(guild.roles, name="Overwatch")
				await discord.utils.get(guild.members, id=payload.user_id).remove_roles(role)
			elif emoji =='7⃣':
				role = discord.utils.get(guild.roles, name="Blobby Volley")
				await discord.utils.get(guild.members, id=payload.user_id).remove_roles(role)
			elif emoji =='8⃣':
				role = discord.utils.get(guild.roles, name="CIV 6")
				await discord.utils.get(guild.members, id=payload.user_id).remove_roles(role)
			elif emoji =='9⃣':
				role = discord.utils.get(guild.roles, name="Flash Games")
				await discord.utils.get(guild.members, id=payload.user_id).remove_roles(role)
			elif emoji =='🔟':
				role = discord.utils.get(guild.roles, name="League of Legends")
				await discord.utils.get(guild.members, id=payload.user_id).remove_roles(role)
			
def setup(bot):
	bot.add_cog(Gamerole(bot))