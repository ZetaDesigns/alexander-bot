from discord.ext import commands
import discord
import random
import os
class Voice(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        if before.channel == None:
            role = discord.utils.get(member.guild.roles, name="Voice Chat")
            await member.add_roles(role)
        elif after.channel == None:
            role = discord.utils.get(member.guild.roles, name="Voice Chat")
            await member.remove_roles(role)
def setup(bot):
    bot.add_cog(Voice(bot))