from discord.ext import commands
import discord
from datetime import datetime
import re
import asyncio
import math
from .db import db_execute, db_get

class controlcenter(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        guild = discord.utils.get(self.bot.guilds, id=434009965988937728)
        controllboard = discord.utils.get(guild.channels, id=508053072098885632)
        async for x in controllboard.history(limit=10):
            await x.delete()
        embed = discord.Embed(
            title = 'Control Center',
            description = 'Hallo, hier im Control Center könnt ihr euer Profil abfragen. Einfach auf die jeweilige Reaction drücken. Viel Erfolg :)',
            colour = discord.Colour.blue()
        )
        embed.set_footer(text='Der Senat')
        embed.set_author(name='Pax Romana', icon_url='')
        embed.add_field(name='Abfragen', value=':white_check_mark: Level\n:triangular_flag_on_post: Verwarnungen',inline=False)
        embed.set_thumbnail(url="https://cdn.discordapp.com/attachments/501126623727452160/507851150100529152/Pax_Romana_2.png")
        xpmsg = await controllboard.send(embed=embed)
        await xpmsg.add_reaction("✅")
        await xpmsg.add_reaction("\N{TRIANGULAR FLAG ON POST}")
    
    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        if payload.channel_id == 508053072098885632 and payload.user_id != 500349811879444500:
            userid = payload.user_id
            guild = discord.utils.get(self.bot.guilds, id=payload.guild_id)
            textchannel = discord.utils.get(guild.channels, id=payload.channel_id)
            user = discord.utils.get(guild.members, id=userid)
            if str(payload.emoji) == '🚩':
                warns = db_get("SELECT reason FROM warn WHERE id = '{}'".format(user.id))
                ids = db_get("SELECT number FROM warn WHERE id = '{}'".format(user.id))
                warnembed = discord.Embed(
                    colour=discord.Colour.red()
                )
                if warns == []:
                    warnembed.description = "Es gibt keine!"
                for count, (warn, id) in enumerate(zip(warns, ids)):
                    if warn[0] != "":
                        warnembed.add_field(name=str(id[0])+":", value=warn[0], inline=False)
                    else:
                        warnembed.add_field(name=str(id[0])+":", value="N/A", inline=False)
                warnembed.set_author(name="Verwarnungen von "+user.name+"#"+user.discriminator, icon_url=user.avatar_url)
                warnmsg = await textchannel.send(embed=warnembed)
                async for x in textchannel.history(limit=10):
                    if(x.id == payload.message_id):
                        message = x
                await message.remove_reaction("🚩", discord.utils.get(guild.members, id=payload.user_id))
                await asyncio.sleep(3)
                await warnmsg.delete()
            elif str(payload.emoji) == '✅':
                username = user.name
                pattern = re.compile('\W')
                username = re.sub(pattern, '', username)
                xp = db_get("SELECT xp FROM benutzer WHERE id = " + str(userid))
                if xp != []:
                    lvl = db_get("SELECT level FROM benutzer WHERE id = " + str(userid))[0][0]
                    xp = xp[0][0]
                else:
                    lvl = 1
                    xp = 0
                    db_execute("REPLACE INTO benutzer (username, id, xp, level) VALUES ('" + username + "', " + str(userid) + ", 0, 1)")
                embed = discord.Embed(
                    title = 'Dein Profil:',
                    colour = discord.Colour.blue()
                    )
                embed.add_field(name='Level:', value= str(lvl), inline=True)
                embed.add_field(name='XP:', value= str(xp), inline=True)
                embed.set_author(name = username, icon_url=user.avatar_url)
                xpmsg = await textchannel.send(embed=embed)
                async for x in textchannel.history(limit=10):
                    if(x.id == payload.message_id):
                        message = x
                await message.remove_reaction("✅", discord.utils.get(guild.members, id=payload.user_id))
                await asyncio.sleep(3)
                await xpmsg.delete()

def setup(bot):
    bot.add_cog(controlcenter(bot))