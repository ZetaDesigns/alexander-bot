import mysql.connector
import yaml

config = yaml.safe_load(open('config.yml'))

mydb = mysql.connector.connect(
	host=config['hostname'],
	user=config['username'],
	passwd=config['password'],
	database="ALEXANDER"
)
mycursor = mydb.cursor()

def db_login():
	mydb = mysql.connector.connect(
	host=config['hostname'],
	user=config['username'],
	passwd=config['password'],
	database="ALEXANDER"
)

def db_execute(sql):
	mycursor.execute(sql)
	mydb.commit()

def db_get(sql):
	mycursor.execute(sql)
	return mycursor.fetchall()