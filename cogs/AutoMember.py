from discord.ext import commands
import discord
from datetime import datetime
class AutoMember(commands.Cog):
	def __init__(self, bot):
		self.bot = bot
    
	@commands.Cog.listener()
	async def on_member_join(self, member):
		logchannel = discord.utils.get(member.guild.channels, id=498063418302136320)
		role = discord.utils.get(member.guild.roles, name="Legionär")
		embed = discord.Embed(color=discord.Color.green(), title=member.name + " ist dem Server beigetreten", timestamp=datetime.utcnow())
		embed.set_thumbnail(url=member.avatar_url)
		embed.add_field(name="Nutzer", value=member.name + "#" + member.discriminator)
		embed.add_field(name="Account erstellt", value=member.created_at.__format__('%A, %d. %B %Y @ %H:%M:%S'))
		embed.set_footer(text="ID: " + str(member.id))
		await member.add_roles(role)
		#await discord.utils.get(member.guild.channels, id=503234579117637632).send("Ave Caesar, **" + member.name+ "** grüßt dich!")
		welcome=discord.Embed(title=" ", color=discord.Color.green())
		welcome.set_author(name="Ave Caesar, "+ member.name+" grüßt dich!", icon_url=member.avatar_url)
		welcomechannel = discord.utils.get(member.guild.channels, id=503234579117637632)
		await welcomechannel.send(embed=welcome)
		await logchannel.send(embed=embed)
	
	@commands.Cog.listener()
	async def on_member_remove(self, member):
		logchannel = discord.utils.get(member.guild.channels, id=498063418302136320)
		embed = discord.Embed(color=discord.Color.red(), title=member.name + " hat den Server verlassen", timestamp=datetime.utcnow())
		embed.set_thumbnail(url=member.avatar_url)
		embed.add_field(name="Nutzer", value=member.name + "#" + member.discriminator)
		embed.add_field(name="Account erstellt", value=member.created_at.__format__('%A, %d. %B %Y @ %H:%M:%S'))
		embed.set_footer(text="ID: " + str(member.id))
		#await discord.utils.get(member.guild.channels, id=503234579117637632).send("Lebe wohl, **" + member.name+ "**!")
		goodbye=discord.Embed(title=" ", color=discord.Color.red())
		goodbye.set_author(name="Lebe wohl, "+ member.name+"!", icon_url=member.avatar_url)
		welcomechannel = discord.utils.get(member.guild.channels, id=503234579117637632)
		await welcomechannel.send(embed=goodbye)
		await logchannel.send(embed=embed)

def setup(bot):
	bot.add_cog(AutoMember(bot))
