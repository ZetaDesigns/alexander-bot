from discord.ext import commands
import discord
import random
import asyncio

class BlackJack(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    global Player1, Player2, Player1Points, Player2Points, p1channel, p2channel, game_halt, p1finish, p2finish, startembed, carddeck
    Player1 = Player2 = p1channel = p2channel = startembed = None
    Player1Points = Player2Points = 0
    game_halt = p1finish = p2finish = False
    carddeck = []

    def prepare_cards(self):
        global carddeck

        for i in range(1, 4):
            for i in range(2, 10):
                carddeck.append(i)
            carddeck.append(10)
            carddeck.append(11)

    def draw_card(self, points):
        global carddeck
        points += random.choice(carddeck)
        return points

    @commands.Cog.listener()
    async def on_ready(self):
        global startembed
        self.prepare_cards()
        startembed = discord.Embed(
            title = 'BlackJack',
            colour = discord.Colour.blue()
            )
        startembed.add_field(name=':busts_in_silhouette: **Teilnehmer** 0/2:', value="────────────\nKeine\n────────────", inline=True)
        startembed.add_field(name='Befehle', value=":white_check_mark: Teilnehmen\n:question: Spielanleitung", inline=False)
        guild = discord.utils.get(self.bot.guilds, id=434009965988937728)
        blackjackchannel = discord.utils.get(guild.channels, id=510851844655022080)
        async for x in blackjackchannel.history(limit=10):
            await x.delete()
        msg = await blackjackchannel.send(embed=startembed)
        await msg.add_reaction("✅")
        await msg.add_reaction("❓")

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        global Player1, Player2, p1channel, p2channel, Player1Points, Player2Points, game_halt, p1finish, p2finish, startembed
        guild = discord.utils.get(self.bot.guilds, id=434009965988937728)
        blackjackmasterchannel = discord.utils.get(guild.channels, name="blackjack")
        Spielanleitung = discord.Embed(
            title='Spielanleitung',
            colour=discord.Colour.blue(),
            description = """
            --------------------------
            Ziel des Spiels: 
            Jeder Spieler versucht durch Karten ziehen so nah wie möglich an die 21 zu gelangen.
            ---------------------------

            --------------------------
            Wie wird gespielt:
            Neben dem Namen des Spielers steht die Summe aller Karten.
            Durch drücken von ✅, wird eine neue Karte gezogen und ihr Kartenwert der momentanen Summe hinzuaddiert!
            Überschreitet die Summe die 21, so hat sich der Spieler überkauft und scheidet aus.
            Durch drücken von ❌ kann man aussteigen und legt sich somit auf die aktuelle Summe fest.
            Sobald alle Spieler ausgestiegen sind, gewinnt der Spieler, welcher am nächsten an der 21 liegt, ohne diese überschritten zu haben.
            -----------------------------""",
            )
        if payload.channel_id == 510851844655022080 and str(payload.emoji) == '❓' and payload.user_id != self.bot.user.id:
            async for x in blackjackmasterchannel.history(limit=10):
                if(x.id == payload.message_id):
                    message = x
            await message.remove_reaction("❓", discord.utils.get(guild.members, id=payload.user_id))
            
            anleitungmsg = await blackjackmasterchannel.send(embed=Spielanleitung)
            await asyncio.sleep(20)
            await anleitungmsg.delete()
        if payload.channel_id == 510851844655022080 and str(payload.emoji) == '✅' and payload.user_id != self.bot.user.id and game_halt == False:
            async for x in blackjackmasterchannel.history(limit=10):
                if(x.id == payload.message_id):
                    message = x
            if Player1 == None:
                Player1 = discord.utils.get(guild.members, id=payload.user_id)
                embed = discord.Embed(
                title = 'BlackJack',
                colour = discord.Colour.blue()
                )
                embed.add_field(name=':busts_in_silhouette: **Teilnehmer** 1/2:', value="────────────\n" + Player1.name + "\n────────────", inline=True)
                embed.add_field(name='Befehle', value=":white_check_mark: Teilnehmen\n:question: Spielanleitung", inline=False)
                await message.remove_reaction("✅", discord.utils.get(guild.members, id=payload.user_id))
                await message.edit(embed=embed)
            elif Player2 == None and payload.user_id != Player1.id:
                Player2 = discord.utils.get(guild.members, id=payload.user_id)
                embed = discord.Embed(
                title = 'BlackJack',
                colour = discord.Colour.blue()
                )
                embed.add_field(name=':busts_in_silhouette: **Teilnehmer** 2/2:', value="────────────\n" + Player1.name + "\n" + Player2.name + "\n────────────", inline=True)
                embed.add_field(name='Befehle', value=":white_check_mark: Teilnehmen\n:question: Spielanleitung", inline=False)
                await message.remove_reaction("✅", discord.utils.get(guild.members, id=payload.user_id))
                await message.remove_reaction("✅", discord.utils.get(guild.members, id=self.bot.user.id))
                await message.edit(embed=embed)
                overwrites1 = {
                    guild.default_role: discord.PermissionOverwrite(read_messages=False),
                    guild.me: discord.PermissionOverwrite(read_messages=True),
                    Player1: discord.PermissionOverwrite(read_messages=True, send_messages=False)
                }
                overwrites2 = {
                    guild.default_role: discord.PermissionOverwrite(read_messages=False),
                    guild.me: discord.PermissionOverwrite(read_messages=True),
                    Player2: discord.PermissionOverwrite(read_messages=True, send_messages=False)
                }
                p1channel = await guild.create_text_channel('blackjack-' + Player1.name, overwrites=overwrites1,category=discord.utils.get(guild.categories, name="minispiele"))
                p2channel = await guild.create_text_channel('blackjack-' + Player2.name, overwrites=overwrites2,category=discord.utils.get(guild.categories, name="minispiele"))
                embed = discord.Embed(
                    title = 'Das Spiel wurde gestartet!',
                    colour = discord.Colour.red()
                )
                embed.add_field(name="Deine Punkte:", value=str(Player1Points) + "/21")
                embed.add_field(name=Player2.name + "'s Punkte:", value="*/21")
                embed.set_footer(text="Drücke auf ✅ um eine Karte zu ziehen")
                p1msg = await p1channel.send(embed=embed)
                embed = discord.Embed(
                    title = 'Das Spiel wurde gestartet!',
                    colour = discord.Colour.red()
                )
                embed.add_field(name="Deine Punkte:", value=str(Player2Points) + "/21")
                embed.add_field(name=Player1.name + "'s Punkte:", value="*/21")
                embed.set_footer(text="Drücke auf ✅ um eine Karte zu ziehen")
                p2msg = await p2channel.send(embed=embed)
                await p1msg.add_reaction("✅")
                await p1msg.add_reaction("❌")
                await p2msg.add_reaction("✅")
                await p2msg.add_reaction("❌")
        elif p1channel != None and payload.channel_id == p1channel.id and payload.user_id != self.bot.user.id and game_halt == False:
            if str(payload.emoji) == '✅':
                Player1Points = self.draw_card(Player1Points)
                async for x in p1channel.history(limit=10):
                    msg = x
                embed = discord.Embed(
                    title = 'Das Spiel wurde gestartet!',
                    colour = discord.Colour.red()
                )
                embed.add_field(name="Deine Punkte:", value=str(Player1Points) + "/21")
                embed.add_field(name=Player2.name + "'s Punkte:", value="*/21")
                embed.set_footer(text="Drücke auf ✅ um eine Karte zu ziehen")
                await msg.edit(embed=embed)
                await msg.remove_reaction("✅", discord.utils.get(guild.members, id=Player1.id))
                if Player1Points > 21:
                    await self.win(2, False)
            elif str(payload.emoji) == '❌':
                async for x in p1channel.history(limit=10):
                    msg = x
                embed = discord.Embed(
                    title = 'Das Spiel wurde gestartet!',
                    colour = discord.Colour.red()
                )
                embed.add_field(name="Deine Punkte:", value=str(Player1Points) + "/21")
                embed.add_field(name=Player2.name + "'s Punkte:", value="*/21")
                await msg.edit(embed=embed)
                await msg.clear_reactions()
                p1finish = True
                if p1finish == True and p2finish == True:
                    if Player1Points > Player2Points:
                        await self.win(1, False)
                    elif Player2Points > Player1Points:
                        await self.win(2, False)
                    elif Player1Points == Player2Points:
                        await self.win(0, True)
        elif p2channel != None and payload.channel_id == p2channel.id and payload.user_id != self.bot.user.id and game_halt == False:
            if str(payload.emoji) == '✅':
                Player2Points = self.draw_card(Player2Points)
                async for x in p2channel.history(limit=10):
                    msg = x
                embed = discord.Embed(
                    title = 'Das Spiel wurde gestartet!',
                    colour = discord.Colour.red()
                )
                embed.add_field(name="Deine Punkte:", value=str(Player2Points) + "/21")
                embed.add_field(name=Player1.name + "'s Punkte:", value="*/21")
                embed.set_footer(text="Drücke auf ✅ um eine Karte zu ziehen")
                await msg.edit(embed=embed)
                await msg.remove_reaction("✅", discord.utils.get(guild.members, id=Player2.id))
                if Player2Points > 21:
                    await self.win(1, False)
            elif str(payload.emoji) == '❌':
                async for x in p2channel.history(limit=10):
                    msg = x
                embed = discord.Embed(
                    title = 'Das Spiel wurde gestartet!',
                    colour = discord.Colour.red()
                )
                embed.add_field(name="Deine Punkte:", value=str(Player2Points) + "/21")
                embed.add_field(name=Player1.name + "'s Punkte:", value=str(Player1Points) + "/21")
                await msg.edit(embed=embed)
                await msg.clear_reactions()
                p2finish = True
                if p1finish == True and p2finish == True:
                    if Player1Points > Player2Points:
                        await self.win(1, False)
                    elif Player2Points > Player1Points:
                        await self.win(2, False)
                    elif Player1Points == Player2Points:
                        await self.win(0, True)

    async def win(self, playerwon, tie):
        global startembed
        winembed = discord.Embed(
            title = 'Spielergebnis:',
            colour = discord.Colour.green()
            )
        winembed.add_field(name="Gewonnen", value="Du hast das Spiel gewonnen.")
        loseembed = discord.Embed(
            title = 'Spielergebnis:',
            colour = discord.Colour.red()
            )
        loseembed.add_field(name="Verloren", value="Dein Gegner hat das Spiel gewonnen.")
        tieembed = discord.Embed(
            title = 'Spielergebnis:',
            colour = 0xf4c542
            )
        tieembed.add_field(name="Unentschieden", value="Du hast gleich viele Punkte wie dein Gegner.")
        global Player1, Player2, p1channel, p2channel, Player1Points, Player2Points, game_halt, p1finish, p2finish
        guild = discord.utils.get(self.bot.guilds, id=434009965988937728)
        blackjackmasterchannel = discord.utils.get(guild.channels, id=510851844655022080)
        game_halt = True
        async for x in p1channel.history(limit=10):
            msg = x
        embed = discord.Embed(
            title = 'Das Spiel wurde gestartet!',
            colour = discord.Colour.red()
        )
        embed.add_field(name="Deine Punkte:", value=str(Player1Points) + "/21")
        embed.add_field(name=Player2.name + "'s Punkte:", value=str(Player2Points) + "/21")
        await msg.edit(embed=embed)
        async for x in p2channel.history(limit=10):
            msg = x
        embed = discord.Embed(
            title = 'Das Spiel wurde gestartet!',
            colour = discord.Colour.red()
        )
        embed.add_field(name="Deine Punkte:", value=str(Player2Points) + "/21")
        embed.add_field(name=Player1.name + "'s Punkte:", value=str(Player1Points) + "/21")
        await msg.edit(embed=embed)
        if tie:
            await p1channel.send(embed=tieembed)
            await p2channel.send(embed=tieembed)
        elif playerwon == 1:
            await p1channel.send(embed=winembed)
            await p2channel.send(embed=loseembed)
        elif playerwon == 2:
            await p1channel.send(embed=loseembed)
            await p2channel.send(embed=winembed)
        await asyncio.sleep(7)
        await p2channel.delete()
        await p1channel.delete()
        Player1 = Player2 = p1channel = p2channel = None
        Player1Points = Player2Points = 0
        guild = discord.utils.get(self.bot.guilds, id=434009965988937728)
        blackjackchannel = discord.utils.get(guild.channels, id=510851844655022080)
        async for x in blackjackchannel.history(limit=10):
            await x.delete()
        msg = await blackjackchannel.send(embed=startembed)
        await msg.add_reaction("✅")
        await msg.add_reaction("❓")
        game_halt = p1finish = p2finish = False

def setup(bot):
    bot.add_cog(BlackJack(bot))