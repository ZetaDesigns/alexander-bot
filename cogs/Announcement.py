from discord.ext import commands
import discord

class Announcement(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['announce', 'ankündigung'])
    async def ankündigen(self, ctx, game: str = "", date: str="", time: str="", *, price: str=""):
        ancEmbed = discord.Embed(color=discord.Color.green(), timestamp=ctx.message.created_at)
        ancEmbed.set_author(name="Ankündigung | " + ctx.message.author.name)
        if game=="":
            return await ctx.send("⚠ Du musst ein Spiel angeben!")
        elif date=="":
            return await ctx.send("⚠ Du musst ein gültiges Datum angeben! Format: DD.MM.YYYY")
        elif time=="":
            return await ctx.send("⚠ Du musst eine Uhrzeit angeben! Format: HH:MM")
        ancEmbed.add_field(name="Event", value=game)
        ancEmbed.add_field(name="Wann?", value=date + " {} Uhr".format(time), inline = True)
        if price!="":
            ancEmbed.add_field(name="Preis", value=price, inline = False)
        ancEmbed.set_footer(text="Drücke ✅ wenn du teilnehmen möchtest!")
        roleStr = ""
        try:
            roleStr = "{}".format(discord.utils.get(ctx.guild.roles, name=game).mention)
        except Exception as e:
        	roleStr = ""
        msg = await ctx.send(roleStr, embed=ancEmbed)
        await msg.add_reaction("✅")
def setup(bot):
    bot.add_cog(Announcement(bot))