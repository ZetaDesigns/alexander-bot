import discord
from discord.ext import commands
from discord.ext.commands import has_permissions, MissingPermissions
class Mod(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    
    @commands.command(pass_context=True)
    @has_permissions(kick_members=True)
    @commands.bot_has_permissions(kick_members=True)
    async def kick(self, ctx, member: discord.Member, *, reason: str = None):
        if member:
            if ctx.message.author.top_role.position < member.top_role.position + 1:
                return await ctx.send("⚠ Du kannst diesen Nutzer nicht kicken da du nicht höher in der Rangliste stehst!")
            else:
                await member.kick(reason=reason)
                return_msg = "Nutzer {}".format(member.mention)
                if reason:
                    return_msg += " wegen: `{}`".format(reason)
                return_msg += " gekickt."
                await ctx.send(return_msg)

    @commands.command(pass_context=True)
    @has_permissions(ban_members=True)
    @commands.bot_has_permissions(kick_members=True)
    async def ban(self, ctx, member: discord.Member, *, reason: str = None):
        if member:
            if ctx.message.author.top_role.position < member.top_role.position + 1:
                return await ctx.send("⚠ Du kannst diesen Nutzer nicht bannen da du nicht höher in der Rangliste stehst!")
            else:
                await member.ban(reason=reason)
                return_msg = "Nutzer {}".format(member.mention)
                if reason:
                    return_msg += " wegen: `{}`".format(reason)
                return_msg += " gebannt."
                await ctx.send(return_msg)

    @commands.command()
    @has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    @commands.guild_only()
    async def clear(self, ctx, *, amount=None):
        if not amount:
            return await ctx.send("❌ Du musst eine Anzahl angeben!")
        elif int(amount) > 500:
            return await ctx.send("❌ Die Anzahl darf nicht größer als 500 sein!")
        try:
            async for x in ctx.channel.history(limit=int(amount)):
                await x.delete()
        except:
            return await ctx.send("❌ Fehler beim löschen der Nachrichten.")
            
    @commands.command(aliases=['ui'])
    @commands.guild_only()
    async def userinfo(self, ctx, *, raw_member=None):
        if not raw_member:
            member = ctx.author
        else:
            try:
                member = await commands.MemberConverter().convert(ctx, raw_member)
            except commands.BadArgument:
                return await ctx.send("❌ Nutzer nicht gefunden.")
        embed = discord.Embed(title="{} ({})".format(member, member.display_name), color=member.top_role.color, timestamp=ctx.message.created_at)
        embed.set_author(name="Nutzer Info")
        embed.add_field(name="ID", value=member.id).add_field(name="Status", value=member.status)
        embed.add_field(name="Aktivität", value=member.activity)
        if len(member.roles) > 1:
            embed.add_field(name="Rollen", value=", ".join([str(role) for role in member.roles[1:]]), inline=False)
        else:
            embed.add_field(name="Rollen", value="None", inline=False)
        embed.add_field(name="Account erstellt", value=member.created_at.__format__('%A, %d. %B %Y @ %H:%M:%S'))
        embed.add_field(name="Beigetreten", value=member.joined_at.__format__('%A, %d. %B %Y @ %H:%M:%S'))
        await ctx.send(embed=embed)

    @commands.command(aliases=['sprich', 'sag'])
    @commands.guild_only()
    async def say(self, ctx, textchannel: commands.TextChannelConverter, *, message: str=""):
    	await textchannel.send(message)

    @ban.error
    @kick.error
    @clear.error
    async def mod_action_handler(self, ctx, error):
            if isinstance(error, commands.BadArgument):
                await ctx.send("❌ Nutzer nicht gefunden.")
            elif isinstance(error, commands.BotMissingPermissions):
                await ctx.send("⚠ Ich habe keine Berechtigung dafür.")
            elif isinstance(error, commands.MissingPermissions):
                await ctx.send("{} du hast keine Berechtigung diesen Befehl auszuführen.".format(ctx.message.author.mention))
            else:
                if ctx.command:
                    await ctx.send("Ein Fehler ist aufgetreten während dem ausführen des `{}` Befehls.".format(ctx.command.name))

def setup(bot):
    bot.add_cog(Mod(bot))