import discord
from discord.ext import commands
from .db import db_execute, db_get, mycursor
import sched, time
import asyncio
from datetime import datetime

class Leaderboard(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    
    loop = asyncio.get_event_loop()

    @commands.Cog.listener()
    async def on_ready(self):
        guild = discord.utils.get(self.bot.guilds, id=434009965988937728)
        leaderboardchannel = discord.utils.get(guild.channels, id=543803499536973835)
        
        async for x in leaderboardchannel.history(limit=10):
            await x.delete()
        
        task = self.loop.create_task(self.start_leaderboard(60))

    @commands.command(pass_context=True)
    async def leaderboard(self, ctx):
        try:
            task.cancel()
        except:
            pass
        guild = discord.utils.get(self.bot.guilds, id=434009965988937728)
        leaderboardchannel = discord.utils.get(guild.channels, id=543803499536973835)
        
        async for x in leaderboardchannel.history(limit=10):
            await x.delete()
        
        task = self.loop.create_task(self.start_leaderboard(60))

    @commands.Cog.listener()
    async def start_leaderboard(self, delay):
        while True:
            guild = discord.utils.get(self.bot.guilds, id=434009965988937728)
            leaderboardchannel = discord.utils.get(guild.channels, id=543803499536973835)

            users = None

            embed = discord.Embed(
                title = 'Leaderboard',
                colour = discord.Colour.blue(),
                timestamp = datetime.utcnow()
            )


            embed.set_footer(text='Der Senat')
            embed.set_author(name='Pax Romana', icon_url='')
            embed.set_thumbnail(url="https://cdn.discordapp.com/attachments/501126623727452160/507851150100529152/Pax_Romana_2.png")
            
            message = None
            async for x in leaderboardchannel.history(limit=1):
                if x.author.id != self.bot.user.id:
                    await x.delete()
                else:
                    message = x
            leaderboardnames = ""
            leaderboardxp = ""
            
            position = 1
            mycursor.execute("SELECT xp, username FROM benutzer ORDER BY xp DESC LIMIT 15")
            users = mycursor.fetchall()
            
            print(users)
            for user in users:
                leaderboardnames += "**" + str(position) + "**" + ". " + str(user[1]) + "\n"
                leaderboardxp += str(user[0]) + "\n"
                position += 1
            
            embed.add_field(name='Name', value=leaderboardnames,inline=True)
            embed.add_field(name='RP', value=leaderboardxp,inline=True)    
            if message is None:
                lbmsg = await leaderboardchannel.send(embed=embed)
            else:
                lbmsg = await message.edit(embed=embed)
            await asyncio.sleep(delay)
def setup(bot):
    bot.add_cog(Leaderboard(bot))