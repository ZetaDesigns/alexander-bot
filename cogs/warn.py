import discord
from discord.ext import commands
from discord.ext.commands import has_permissions, MissingPermissions
from .db import db_execute, db_get, mycursor
class Warn(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    
    @commands.command(pass_context=True)
    @has_permissions(kick_members=True)
    async def warn(self, ctx, member: discord.Member = None, *, reason: str = ""):
        warns = db_get("SELECT reason FROM warn WHERE id = '{}'".format(member.id))
        if member == None:
            return await ctx.send("❌ Du musst einen Nutzer angeben!")
        if member:
            if ctx.message.author.top_role.position < member.top_role.position + 1:
                return await ctx.send("⚠ Du kannst diesen Nutzer nicht verwarnen da du nicht höher in der Rangliste stehst!")
            else:
                mycursor.execute("INSERT INTO warn (username, id, reason, number) VALUES (%s,%s,%s,%s);", (member.name, str(member.id), reason, str(len(warns) + 1)))
                return_msg = "Nutzer {}".format(member.mention)
                if reason:
                    return_msg += " wegen: `{}`".format(reason)
                return_msg += " verwarnt."
                await ctx.send(return_msg)
    @commands.command(pass_context=True)
    @has_permissions(kick_members=True)
    async def listwarns(self, ctx, member: discord.Member):
        warns = db_get("SELECT reason FROM warn WHERE id = '{}'".format(member.id))
        ids = db_get("SELECT number FROM warn WHERE id = '{}'".format(member.id))
        warnembed = discord.Embed(
            colour=discord.Colour.red()
        )
        if warns == []:
            warnembed.description = "Es gibt keine!"
        for count, (warn, id) in enumerate(zip(warns, ids)):
            if warn[0] != "":
                warnembed.add_field(name=str(id[0])+":", value=warn[0], inline=False)
            else:
                warnembed.add_field(name=str(id[0])+":", value="N/A", inline=False)
        warnembed.set_author(name="Verwarnungen von "+member.name+"#"+member.discriminator, icon_url=member.avatar_url)
        await ctx.send(embed=warnembed)
    
    @commands.command(pass_context=True)
    @has_permissions(kick_members=True)
    async def clearwarns(self, ctx, member: discord.Member):
        db_execute("DELETE FROM warn WHERE id = '{}'".format(member.id))
        await ctx.send("Alle Verwarnungen von " + member.mention + " wurden gelöscht.")
    
    @commands.command(pass_context=True)
    @has_permissions(kick_members=True)
    async def removewarn(self, ctx, member: discord.Member, index: int):
        warns = db_get("SELECT reason FROM warn WHERE id = '{}'".format(member.id))
        if warns == []:
            await ctx.send(member.mention + " hat überhaupt keine Verwarnungen!")
        else:
            db_execute("DELETE FROM warn WHERE id = '{}' AND number = '{}'".format(member.id, index))
            ctx.send("Verwarnung " + str(index) + " von " + member.name + " wurde gelöscht.")
    @clearwarns.error
    @listwarns.error
    @removewarn.error
    @warn.error
    async def mod_action_handler(self, ctx, error):
            if isinstance(error, commands.BadArgument):
                await ctx.send("❌ Nutzer nicht gefunden.")
            elif isinstance(error, commands.MissingPermissions):
                await ctx.send("{} du hast keine Berechtigung diesen Befehl auszuführen.".format(ctx.message.author.mention))
            else:
                if ctx.command:
                    await ctx.send("Ein Fehler ist aufgetreten während dem ausführen des `{}` Befehls: ```{}```".format(ctx.command.name, error))

def setup(bot):
    bot.add_cog(Warn(bot))